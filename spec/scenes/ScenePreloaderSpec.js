import { PreloadEngineSpy } from '../support/engineSpies/PreloadEngineSpy.js'
import { PlatformAsset } from '../../src/scenes/assets/PlatformAsset.js'
import { ScenePreloader } from '../../src/scenes/ScenePreloader.js'
import { DudeAsset } from '../../src/scenes/assets/DudeAsset.js'
import { SkyAsset } from '../../src/scenes/assets/SkyAsset.js'

describe('Preload scene', () => {
  it('loads images', () => {
    const engineSpy = new PreloadEngineSpy()

    new ScenePreloader(engineSpy).preload()

    expect(engineSpy.loadedImage(SkyAsset.name())).toEqual(SkyAsset.file())
    expect(engineSpy.loadedImage(PlatformAsset.name())).toEqual(PlatformAsset.file())
  })

  it('loads spritesheet', () => {
    const engineSpy = new PreloadEngineSpy()

    new ScenePreloader(engineSpy).preload()

    expect(engineSpy.loadedSpritesheetName()).toEqual(DudeAsset.name())
    expect(engineSpy.loadedSpritesheetFile()).toEqual(DudeAsset.file())
    expect(engineSpy.loadedSpritesheetFrameSize()).toEqual(DudeAsset.frameSize())
  })
})
