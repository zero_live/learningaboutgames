import { CreateEngineSpy } from '../support/engineSpies/CreateEngineSpy.js'
import { PlatformAsset } from '../../src/scenes/assets/PlatformAsset.js'
import { VariableSharer } from '../../src/utilities/VariableSharer.js'
import { customExpect } from '../support/customExpectToBeBetween.js'
import { DudeAsset } from '../../src/scenes/assets/DudeAsset.js'
import { SceneCreator } from '../../src/scenes/SceneCreator.js'
import { SkyAsset } from '../../src/scenes/assets/SkyAsset.js'
import { Config } from '../../src/utilities/Config.js'

describe('SceneCreator', () => {
  let engineSpy, scene, sharer

  beforeEach(() => {
    engineSpy = new CreateEngineSpy()
    sharer = new VariableSharer()
  })

  describe('after execution', () => {
    beforeEach(() => {
      scene = new SceneCreator(engineSpy, sharer)
      scene.create()
    })

    it('presents background', () => {

      expect(engineSpy.loadedImageName()).toBe(SkyAsset.name())
      expect(engineSpy.loadedImageInitialX()).toBe(SkyAsset.initialX())
      expect(engineSpy.loadedImageInitialY()).toBe(SkyAsset.initialY())
    })

    it('presents player', () => {

      expect(engineSpy.loadedSpriteName()).toBe(DudeAsset.name())
      expect(engineSpy.loadedSpriteInitialX()).toBe(DudeAsset.initialX())
      expect(engineSpy.loadedSpriteInitialY()).toBe(DudeAsset.initialY())
    })

    it('presents platforms', () => {

      expect(engineSpy.loadedGroupName()).toBe(PlatformAsset.name())
      expect(engineSpy.loadedGroupElementsCreated()).toBe(20)
      customExpect(engineSpy.loadedGroupInitialX()).toBeBetween(0, Config.width)
      customExpect(engineSpy.loadedGroupInitialY()).toBeBetween(0, Config.height)
    })

    it('declares the collide between player and platform', () => {
      const collide = { someElement: DudeAsset.name(), anotherElement: PlatformAsset.name() }

      expect(engineSpy.declaredColliter()).toEqual(collide)
    })
  })
})
