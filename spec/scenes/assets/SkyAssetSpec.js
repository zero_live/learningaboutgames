import { SkyAsset } from '../../../src/scenes/assets/SkyAsset.js'

describe('SkyAsset', () => {
  it('has properties', () => {
    expect(SkyAsset.name()).toBe('sky')
    expect(SkyAsset.file()).toBe('assets/sky.png')
    expect(SkyAsset.initialX()).toBe(400)
    expect(SkyAsset.initialY()).toBe(300)
  })
})
