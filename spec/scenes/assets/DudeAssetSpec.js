import { DudeAsset } from '../../../src/scenes/assets/DudeAsset.js'

describe('DudeAsset', () => {
  it('has properties', () => {
    const frameSize = { frameWidth: 32, frameHeight: 48 }

    expect(DudeAsset.name()).toBe('dude')
    expect(DudeAsset.file()).toBe('assets/dude.png')
    expect(DudeAsset.frameSize()).toEqual(frameSize)
  })
})
