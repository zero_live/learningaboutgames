import { PlatformAsset } from '../../../src/scenes/assets/PlatformAsset.js'
import { customExpect } from '../../support/customExpectToBeBetween.js'
import { Config } from '../../../src/utilities/Config.js'

describe('PlatformAsset', () => {
  it('has properties', () => {
    const width = 32
    const height = 32
    const centerWidth = width / 2
    const centerHeight = height / 2
    const correction = 1
    const maximumX = (Config.width - centerWidth) + correction
    const maximumY = (Config.height - centerHeight) + correction

    expect(PlatformAsset.name()).toBe('platform')
    expect(PlatformAsset.file()).toBe('assets/platform.png')
    expect(PlatformAsset.width()).toBe(width)
    expect(PlatformAsset.height()).toBe(height)
    expect(PlatformAsset.centerWidth()).toBe(centerWidth)
    expect(PlatformAsset.centerHeight()).toBe(centerHeight)
    customExpect(PlatformAsset.initialX()).toBeBetween(centerWidth, maximumX)
    customExpect(PlatformAsset.initialY()).toBeBetween(centerHeight, maximumY)
  })
})
