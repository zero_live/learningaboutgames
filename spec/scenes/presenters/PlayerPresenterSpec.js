import { PlayerPresenter } from '../../../src/scenes/presenters/PlayerPresenter.js'
import { CreateEngine } from '../../../src/scenes/engines/CreateEngine.js'
import { CreateEngineSpy } from '../../support/engineSpies/CreateEngineSpy.js'
import { DudeAsset } from '../../../src/scenes/assets/DudeAsset.js'

describe('PlayerPresenter', () => {
  let engineSpy, engine

  beforeEach(() => {
    engineSpy = new CreateEngineSpy()
    engine = new CreateEngine(engineSpy)
  })

  it('adds physics', () => {

    new PlayerPresenter(engine)

    expect(engineSpy.loadedSpriteName()).toBe(DudeAsset.name())
    expect(engineSpy.loadedSpriteInitialX()).toBe(100)
    expect(engineSpy.loadedSpriteInitialY()).toBe(450)
    expect(engineSpy.loadedCollideWorldBounds()).toBe(true)
  })

  it('creates frame animations when is presented', () => {

    new PlayerPresenter(engine).present()

    expect(engineSpy.animationsSize()).toBe(3)
    expect(engineSpy.animationsQuantityOfFrames()).toBe(9)
  })

  describe('after present', () => {
    const playerVelocity = 160
    const leftAnimationName = 'left'
    const rightAnimationName = 'right'
    let player

    beforeEach(() => {
      player = new PlayerPresenter(engine)
      player.present()
    })

    it('stops movement', () => {

      player.stopMoving()

      expect(engineSpy.currentSpriteSheetXVelocity()).toBe(0)
      expect(engineSpy.currentSpriteSheetYVelocity()).toBe(0)
      expect(engineSpy.currentSpriteSheetAnimationName()).toBe('turn')
    })

    it('moves to left', () => {

      player.moveLeft()

      expect(engineSpy.currentSpriteSheetXVelocity()).toBe(-playerVelocity)
      expect(engineSpy.currentSpriteSheetYVelocity()).toBeUndefined()
      expect(engineSpy.currentSpriteSheetAnimationName()).toBe(leftAnimationName)
    })

    it('moves to right', () => {

      player.moveRight()

      expect(engineSpy.currentSpriteSheetXVelocity()).toBe(playerVelocity)
      expect(engineSpy.currentSpriteSheetYVelocity()).toBeUndefined()
      expect(engineSpy.currentSpriteSheetAnimationName()).toBe(rightAnimationName)
    })

    it('moves to up', () => {

      player.moveUp()

      expect(engineSpy.currentSpriteSheetXVelocity()).toBeUndefined()
      expect(engineSpy.currentSpriteSheetYVelocity()).toBe(-playerVelocity)
      expect(engineSpy.currentSpriteSheetAnimationName()).toBe(leftAnimationName)
    })

    it('moves to down', () => {

      player.moveDown()

      expect(engineSpy.currentSpriteSheetXVelocity()).toBeUndefined()
      expect(engineSpy.currentSpriteSheetYVelocity()).toBe(playerVelocity)
      expect(engineSpy.currentSpriteSheetAnimationName()).toBe(rightAnimationName)
    })
  })
})
