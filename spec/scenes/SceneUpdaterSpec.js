import { UpdateEngineSpy } from '../support/engineSpies/UpdateEngineSpy.js'
import { SceneUpdater } from '../../src/scenes/SceneUpdater.js'
import { VariableSharer } from '../../src/utilities/VariableSharer.js'
import { PlayerStub } from '../support/PlayerStub.js'

describe('SceneUpdater', () => {
  let engineSpy, player, sharer

  beforeEach(() => {
    engineSpy = new UpdateEngineSpy()
    sharer = new VariableSharer()
    player = new PlayerStub()
    sharer.add('player', player)
  })

  describe('after update', () => {
    let scene

    beforeEach(() => {
      scene = new SceneUpdater(engineSpy, sharer)
    })

    it('does not move the player when all the cursors are up', () => {

      scene.update()

      expect(engineSpy.isAnyCursorDown()).toBe(false)
      expect(player.isMovingTo()).toBe('none')
    })

    it('moves the player to the left when that cursor is down', () => {
      engineSpy.pressLeftCursor()

      scene.update()

      expect(engineSpy.isAnyCursorDown()).toBe(true)
      expect(player.isMovingTo()).toBe('left')
    })

    it('moves the player to the right when that cursor is down', () => {
      engineSpy.pressRightCursor()

      scene.update()

      expect(engineSpy.isAnyCursorDown()).toBe(true)
      expect(player.isMovingTo()).toBe('right')
    })

    it('moves the player to the down when that cursor is down', () => {
      engineSpy.pressDownCursor()

      scene.update()

      expect(engineSpy.isAnyCursorDown()).toBe(true)
      expect(player.isMovingTo()).toBe('down')
    })

    it('moves the player to the up when that cursor is down', () => {
      engineSpy.pressUpCursor()

      scene.update()

      expect(engineSpy.isAnyCursorDown()).toBe(true)
      expect(player.isMovingTo()).toBe('up')
    })
  })
})
