import { Config } from '../../src/utilities/Config.js'

describe('Config', () => {
  it('includes screen size', () => {

    expect(Config.width).toBe(800)
    expect(Config.height).toBe(600)
  })

  it('includes all the scenes', () => {
    const preload = function() {}
    const create = function() {}
    const update = function() {}

    const config = Config.build(preload, create, update)

    expect(config.scene.preload).toBe(preload)
    expect(config.scene.create).toBe(create)
    expect(config.scene.update).toBe(update)
  })

  it('includes arcade physics', () => {
    const physicsConfig = { default: 'arcade', arcade: { debug: false }}

    const config = Config.build()

    expect(config.physics).toEqual(physicsConfig)
  })
})
