
export const customExpect = (actual) => {
  return {
    toBeBetween(initial, final) {
      expect(actual >= initial).toBe(true)
      expect(actual <= final).toBe(true)
    }
  }
}
