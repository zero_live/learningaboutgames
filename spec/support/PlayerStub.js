
export class PlayerStub {
  NO_DIRECTION = 'none'

  constructor() {
    this.movementDirection = this.NO_DIRECTION
  }

  stopMoving() {
    this.movementDirection = this.NO_DIRECTION
  }

  moveLeft() {
    this.movementDirection = 'left'
  }

  moveRight() {
    this.movementDirection = 'right'
  }

  moveUp() {
    this.movementDirection = 'up'
  }

  moveDown() {
    this.movementDirection = 'down'
  }

  isMovingTo() {
    return this.movementDirection
  }
}
