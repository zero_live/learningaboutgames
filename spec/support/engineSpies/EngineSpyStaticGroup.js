
export class EngineSpyStaticGroup {
  constructor() {
    this.name
    this.initialX
    this.initialY
    this.elementsCreated = 0
  }

  create(initialX, initialY, name) {
    this.elementsCreated ++

    this.name = name
    this.initialX = initialX
    this.initialY = initialY
  }
}
