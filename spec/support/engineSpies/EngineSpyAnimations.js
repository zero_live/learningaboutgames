
export class EngineSpyAnimations {
  constructor() {
    this.list = []
  }

  add(data) {
    this.list.push(data)
  }

  size() {
    return this.list.length
  }

  quantityOfFrames() {
    let countFrames = 0

    this.list.forEach((animation) => {
      countFrames += animation.frames.length
    })

    return countFrames
  }
}
