
export class UpdateEngineSpy {
  constructor() {
    this.cursorKeys

    this.input = { keyboard: { createCursorKeys: this.createCursorKeys.bind(this) } }
  }

  createCursorKeys() {
    this.cursorKeys = new CursorKeysStub()

    return this.cursorKeys
  }

  pressLeftCursor() {
    this.cursorKeys.left.isDown = true
  }

  pressRightCursor() {
    this.cursorKeys.right.isDown = true
  }

  pressDownCursor() {
    this.cursorKeys.down.isDown = true
  }

  pressUpCursor() {
    this.cursorKeys.up.isDown = true
  }

  isAnyCursorDown() {
    return this.cursorKeys.isAnyCursorDown()
  }
}

class CursorKeysStub {
  constructor() {
    this.left = { isDown: false }
    this.up = { isDown: false }
    this.right = { isDown: false }
    this.down = { isDown: false }
  }

  isAnyCursorDown() {
    return this.left.isDown || this.up.isDown || this.right.isDown || this.down.isDown
  }
}
