
export class PreloadEngineSpy {
  constructor() {
    this.loadedImages = {}
    this.spritesheetName
    this.spritesheetFile
    this.spritesheetFrameSize

    this.load = {
      image: this.image.bind(this),
      spritesheet: this.spritesheet.bind(this)
    }
  }

  image(name, file) {
    this.loadedImages[name] = file
  }

  spritesheet(name, file, frameSize) {
    this.spritesheetName = name
    this.spritesheetFile = file
    this.spritesheetFrameSize = frameSize
  }

  loadedSpritesheetName() { return this.spritesheetName }
  loadedSpritesheetFile() { return this.spritesheetFile }
  loadedSpritesheetFrameSize() { return this.spritesheetFrameSize }
  loadedImage(name) { return this.loadedImages[name] }
}
