
export class EngineSpySpriteSheet {
  constructor(initialX, initialY, name) {
    this.name = name
    this.initialX = initialX
    this.initialY = initialY
    this.collideWorldBounds
    this.xVelocity
    this.yVelocity
    this.currentAnimationName
    this.anims = { play: this.playAnimation.bind(this) }
  }

  setCollideWorldBounds(activate) {
    this.collideWorldBounds = activate
  }

  setVelocityX(velocity) {
    this.xVelocity = velocity
  }

  setVelocityY(velocity) {
    this.yVelocity = velocity
  }

  playAnimation(name) {
    this.currentAnimationName = name
  }
}
