import { EngineSpySpriteSheet } from './EngineSpySpriteSheet.js'
import { EngineSpyStaticGroup } from './EngineSpyStaticGroup.js'
import { EngineSpyAnimations } from './EngineSpyAnimations.js'

export class CreateEngineSpy {
  constructor() {
    this.imageName
    this.imageInitialX
    this.imageInitiloadedImageInitialY
    this.spriteSheet
    this.group
    this.collider
    this.animations = new EngineSpyAnimations()

    this.add = { image: this.addImage }
    this.physics = {
      add: {
        sprite: this.addSprite,
        staticGroup: this.addStaticGroup,
        collider: this.declareCollider
      }
    }

    this.anims = {
      create: this.createAnimation
    }
  }

  addImage = (initialX, initialY, name) => {
    this.imageName = name
    this.imageInitialX = initialX
    this.imageInitialY = initialY
  }

  addSprite = (initialX, initialY, name) => {
    this.spriteSheet = new EngineSpySpriteSheet(initialX, initialY, name)

    return this.spriteSheet
  }

  addStaticGroup = () => {
    this.group = new EngineSpyStaticGroup()

    return this.group
  }

  createAnimation = (animation) => {
    this.animations.add(animation)
  }

  declareCollider = (someElement, anotherElement) => {
    this.collider = {
      someElement: someElement.name,
      anotherElement: anotherElement.name
    }
  }

  animationsSize() { return this.animations.size() }
  animationsQuantityOfFrames() { return this.animations.quantityOfFrames() }
  declaredColliter() { return this.collider }
  loadedGroupName() { return this.group.name }
  loadedGroupInitialX() { return this.group.initialX }
  loadedGroupInitialY() { return this.group.initialY }
  loadedGroupElementsCreated() { return this.group.elementsCreated }
  loadedImageName() { return this.imageName }
  loadedImageInitialX() { return this.imageInitialX }
  loadedImageInitialY() { return this.imageInitialY }
  loadedSpriteName() { return this.spriteSheet.name }
  loadedSpriteInitialX() { return this.spriteSheet.initialX }
  loadedSpriteInitialY() { return this.spriteSheet.initialY }
  loadedCollideWorldBounds() { return this.spriteSheet.collideWorldBounds }
  currentSpriteSheetXVelocity() { return this.spriteSheet.xVelocity }
  currentSpriteSheetYVelocity() { return this.spriteSheet.yVelocity }
  currentSpriteSheetAnimationName() { return this.spriteSheet.currentAnimationName }
}
