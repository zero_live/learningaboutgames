
export const randomNumberBetween = (minimum, maximum) => {
  const realMaximum = maximum + 1

  let random = Math.random() * realMaximum
  if (random > realMaximum) { random = realMaximum }
  if (random < minimum) { random = minimum }

  return Math.floor(random)
}
