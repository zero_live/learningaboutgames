
export class VariableSharer {
  add(key, value) {
    this[key] = value
  }

  get(key) {
    return this[key]
  }
}
