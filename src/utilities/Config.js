
export class Config {
  static width = 800
  static height = 600

  static build(preload, create, update) {
    return {
      width: Config.width,
      height: Config.height,
      physics: {
        default: 'arcade',
        arcade: { debug: false }
      },
      scene: {
        preload: preload,
        create: create,
        update: update
      }
    }
  }
}