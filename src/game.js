import { VariableSharer } from './utilities/VariableSharer.js'
import { ScenePreloader } from './scenes/ScenePreloader.js'
import { SceneUpdater } from './scenes/SceneUpdater.js'
import { SceneCreator } from './scenes/SceneCreator.js'
import { Config } from './utilities/Config.js'

const sharer = new VariableSharer()
const config = Config.build(preload, create, update)
new Phaser.Game(config)

function preload() { new ScenePreloader(this).preload() }
function create() { new SceneCreator(this, sharer).create() }
function update () { new SceneUpdater(this, sharer).update() }
