
export class FramesBuilder {
  static build(name, initial, final) {
    const frames = this._buildFrames(name, initial, final)

    return frames
  }

  static _buildFrames(name, initial, final) {
    const frames = []

    for(let i = initial; i <= final; i++) {
      const frame = this._buildFrame(name, i)

      frames.push(frame)
    }

    return frames
  }

  static _buildFrame(name, position) {
    return { key: name, frame: position }
  }
}
