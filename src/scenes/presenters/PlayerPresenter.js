import { DudeAsset } from '../assets/DudeAsset.js'
import { FramesBuilder } from './FrameBuilder.js'

export class PlayerPresenter {
  LEFT_ANIMATION_NAME = 'left'
  TURN_ANIMATION_NAME = 'turn'
  RIGHT_ANIMATION_NAME = 'right'
  YES = -1
  FRAME_RATE = 10
  PLAYER_VELOCITY = 160

  constructor(engine) {
    this.engine = engine

    this._addPhysics()
  }

  present() {
    this._createPlayerAnimations()
  }

  moveLeft() {
    this.spriteSheet.brakeVertically(this.PLAYER_VELOCITY)
    this._playLeftAnimation()
  }

  moveRight() {
    this.spriteSheet.accelerateVertically(this.PLAYER_VELOCITY)
    this._playRightAnimation()
  }

  moveUp() {
    this.spriteSheet.accelerateHorizontally(this.PLAYER_VELOCITY)
    this._playLeftAnimation()
  }

  moveDown() {
    this.spriteSheet.brakeHorizontally(this.PLAYER_VELOCITY)
    this._playRightAnimation()
  }

  stopMoving() {
    this.spriteSheet.stopAcceleration()
    this._playTurnAnimation()
  }

  asPhysic() {
    return this.spriteSheet.asPhysic()
  }

  _playLeftAnimation() {
    this.spriteSheet.playAnimation(this.LEFT_ANIMATION_NAME)
  }

  _playTurnAnimation() {
    this.spriteSheet.playAnimation(this.TURN_ANIMATION_NAME)
  }

  _playRightAnimation() {
    this.spriteSheet.playAnimation(this.RIGHT_ANIMATION_NAME)
  }

  _addPhysics() {
    this.spriteSheet = this.engine.addSprite(DudeAsset)
    this.spriteSheet.enableCollideWorldBounds()
  }

  _createPlayerAnimations() {
    const animations = {
      [this.LEFT_ANIMATION_NAME]: { initial: 0, final: 3 },
      [this.TURN_ANIMATION_NAME]: { initial: 4, final: 4 },
      [this.RIGHT_ANIMATION_NAME]: { initial: 5, final: 8 },
    }

    for(let name in animations) {
      const { initial, final } = animations[name]

      this._createAnimation(name, initial, final)
    }
  }

  _createAnimation(name, initial, final) {
    this.engine.createAnimation({
      key: name,
      frames: this._buildFrames(initial, final),
      frameRate: this.FRAME_RATE,
      repeat: this.YES
    })
  }

  _buildFrames(initial, final) {
    return FramesBuilder.build(DudeAsset.name(), initial, final)
  }
}
