import { PreloadEngine } from './engines/PreloadEngine.js'
import { PlatformAsset } from './assets/PlatformAsset.js'
import { SkyAsset } from './assets/SkyAsset.js'
import { DudeAsset } from './assets/DudeAsset.js'

export class ScenePreloader {
  constructor(engine) {
    this.engine = new PreloadEngine(engine)
  }

  preload() {
    this.engine.loadImage(SkyAsset)
    this.engine.loadImage(PlatformAsset)
    this.engine.loadSpritesheet(DudeAsset)
  }
}
