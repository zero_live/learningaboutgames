
export class EngineStaticGroup {
  constructor(group) {
    this.group = group
  }

  add(asset) {
    this.group.create(asset.initialX(), asset.initialY(), asset.name())
  }

  asPhysic() {
    return this.group
  }
}
