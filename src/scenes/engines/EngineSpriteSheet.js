
export class EngineSpriteSheet {
  constructor(sprite) {
    this.sprite = sprite
  }

  enableCollideWorldBounds() {
    this.sprite.setCollideWorldBounds(true)
  }

  accelerateHorizontally(velocity) {
    this._setVelocityX(velocity)
  }

  accelerateVertically(velocity) {
    this._setVelocityY(velocity)
  }

  brakeHorizontally(velocity) {
    this._setVelocityX(-velocity)
  }

  brakeVertically(velocity) {
    this._setVelocityY(-velocity)
  }

  playAnimation(name) {
    this.sprite.anims.play(name, true)
  }

  stopAcceleration() {
    this.brakeHorizontally(0)
    this.brakeVertically(0)
  }

  asPhysic() {
    return this.sprite
  }

  _setVelocityY(velocity) {
    this.sprite.setVelocityX(velocity)
  }

  _setVelocityX(velocity) {
    this.sprite.setVelocityY(-velocity)
  }
}
