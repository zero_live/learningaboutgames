
export class PreloadEngine {
  constructor(engine) {
    this.engine = engine
  }

  loadImage(asset) {
    this.engine.load.image(asset.name(), asset.file())
  }

  loadSpritesheet(asset) {
    this.engine.load.spritesheet(asset.name(), asset.file(), asset.frameSize())
  }
}
