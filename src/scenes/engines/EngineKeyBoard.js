
export class EngineKeyBoard {
  constructor(engine) {
    this.keys = engine.input.keyboard.createCursorKeys()
  }

  leftIsDown() {
    return this.keys.left.isDown
  }

  rightIsDown() {
    return this.keys.right.isDown
  }

  downIsDown() {
    return this.keys.down.isDown
  }

  upIsDown() {
    return this.keys.up.isDown
  }

  isAnyArrowDown() {
    return this.upIsDown() || this.downIsDown() || this.leftIsDown() || this.rightIsDown()
  }
}
