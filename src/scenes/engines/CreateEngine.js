import { EngineSpriteSheet } from './EngineSpriteSheet.js'
import { EngineStaticGroup } from './EngineStaticGroup.js'

export class CreateEngine {
  constructor(engine) {
    this.engine = engine
  }

  addImage(asset) {
    const image = this.engine.add.image(asset.initialX(), asset.initialY(), asset.name())

    return image
  }

  addSprite(asset) {
    const sprite = this.engine.physics.add.sprite(asset.initialX(), asset.initialY(), asset.name())

    return new EngineSpriteSheet(sprite)
  }

  createAnimation(data) {
    this.engine.anims.create(data)
  }

  addStaticGroup() {
    const group = this.engine.physics.add.staticGroup()

    return new EngineStaticGroup(group)
  }

  addCollider(someElement, anotherElemen) {
    this.engine.physics.add.collider(someElement.asPhysic(), anotherElemen.asPhysic())
  }
}
