import { EngineKeyBoard } from './engines/EngineKeyBoard.js'

export class SceneUpdater {
  constructor(engine, sharer) {
    this.engine = engine
    this.sharer = sharer

    this.keyboard = new EngineKeyBoard(engine)
  }

  update() {
    this._playerMovements()
  }

  _playerMovements() {
    if (this.keyboard.leftIsDown()) { this._player().moveLeft() }
    if (this.keyboard.rightIsDown()) { this._player().moveRight() }
    if (this.keyboard.upIsDown()) { this._player().moveUp() }
    if (this.keyboard.downIsDown()) { this._player().moveDown() }
    if (!this.keyboard.isAnyArrowDown()){ this._player().stopMoving() }
  }

  _player() {
    return this.sharer.get('player')
  }
}
