import { Config } from '../../utilities/Config.js'

export class SkyAsset {
  static name() { return 'sky' }
  static file() { return 'assets/sky.png' }
  static initialX() { return Config.width / 2 }
  static initialY() { return Config.height / 2 }
}
