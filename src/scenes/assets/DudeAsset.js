
export class DudeAsset {
  static name() { return 'dude' }
  static file() { return 'assets/dude.png' }
  static initialX() { return 100 }
  static initialY() { return 450 }
  static file() { return 'assets/dude.png' }
  static frameSize() { return { frameWidth: 32, frameHeight: 48 } }
}
