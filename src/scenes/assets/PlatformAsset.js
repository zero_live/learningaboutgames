import { randomNumberBetween } from '../../utilities/randomNumberBetween.js'
import { Config } from '../../utilities/Config.js'

export class PlatformAsset {
  static name() { return 'platform' }
  static file() { return 'assets/platform.png' }
  static width() { return 32 }
  static height() { return 32 }
  static centerWidth() { return this.width() / 2 }
  static centerHeight() { return this.height() / 2 }
  static initialX() { return randomNumberBetween(this.centerWidth(), Config.width - this.centerWidth()) }
  static initialY() { return randomNumberBetween(this.centerHeight(), Config.height - this.centerHeight()) }
}
