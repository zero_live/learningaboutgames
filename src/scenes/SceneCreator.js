import { PlayerPresenter } from './presenters/PlayerPresenter.js'
import { PlatformAsset } from './assets/PlatformAsset.js'
import { CreateEngine } from './engines/CreateEngine.js'
import { doTimes } from '../utilities/doTimes.js'
import { SkyAsset } from './assets/SkyAsset.js'

export class SceneCreator {
  QUANTITY_OF_PLATFORMS = 20

  constructor(engine, sharer) {
    this.engine = new CreateEngine(engine)
    this.sharer = sharer

    this.player
    this.platforms
  }

  create() {
    this._presentBackground()
    this._presentPlayer()
    this._presentPlatforms()
    this.engine.addCollider(this.player, this.platforms)
  }

  _presentBackground() {
    this.engine.addImage(SkyAsset)
  }

  _presentPlatforms() {
    this.platforms = this.engine.addStaticGroup()
    doTimes(this.QUANTITY_OF_PLATFORMS, () => {
      this.platforms.add(PlatformAsset)
    })
  }

  _presentPlayer() {
    this.player = new PlayerPresenter(this.engine)
    this.player.present()

    this.sharer.add('player', this.player)
  }
}
