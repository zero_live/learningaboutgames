# Learning about Game

A project for learning about Web games.

## System requirements

- `Docker version 19.03.12` or higher.
- `docker-compose version 1.26.2` or higher.
- To have free the port `8080`.

## How to run the project

You have to run the command `sh scripts/up` and visit with your browser `http://localhost:8080/src/`.

When you finish using the project, I recommend you use the command `sh scripts/down`.

## How to run the tests

After run the project, you have to visit with your browser `http://localhost:8080/SpecRunner.html`.

> You can run the tests in the terminal but the information provided if very poor. Run `sh scripts/test` for run tests in the terminal.

## Production environment

After the code pass the CI in master branch, the project is deployed at `https://zero_live.gitlab.io/learningaboutgames/`
